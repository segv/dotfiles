# segv's dotfiles

## Installation for Arch based distros
```bash
git clone https://codeberg.org/segv/dotfiles
cd dotfiles
sudo make larbs
make
```

## Screenshot
![Screenshot](https://codeberg.org/segv/dotfiles/raw/master/img/empty.png)
![Screenshot](https://codeberg.org/segv/dotfiles/raw/master/img/busy.png)

## After install
* Open `nvim`, wait for `packer.nvim` to install, restart `nvim` and run `:PackerSync`
