#!/bin/zsh

umask 022
tty 2>&1 | grep tty >/dev/null 2>&1 && setfont ter-932n.psf.gz
stty start ""

autoload -U colors && colors

# powerline sus
# export PS1='%K{blue}%F{white} %n@%m %F{blue}%K{red}%F{white}%K{red} %1~ %F{red}%K{none}%K{none}%F{white} '
export RPS1='%(?..%K{none}%F{red}%K{red}%F{white}err: %?%F{white})'
# color prompt sus too
precmd() {
    echo "- $(tput setaf 1)`pwd`"
}
export PS1="%F{blue}%n%F{magenta}@%F{green}%m%F{red} %#%F{white} "
#export PS1="$ "
unset RPS1

export HASH_LIST_ALL=1

. ~/.config/shell/exportrc
. ~/.config/shell/aliasrc
. ~/.config/shell/funcrc
[ -f ~/.config/LF_ICONS ] && {
	LF_ICONS="$(tr '\n' ':' <~/.config/LF_ICONS)" \
		&& export LF_ICONS
}

set -h

# basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1

# use vi keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line
set -h

# key map
bindkey -s '^f' '^ufcd\n'
bindkey -s '^o' '^usrccd\n'
bindkey -s '^n' '^ufmcd\n'
bindkey -s '^r' '^ureload\n'
bindkey -s '^h' '^uhf\n'
bindkey -s '^q' '^uchtsh\n'
bindkey -s '^k' 'OA' # up
bindkey -s '^,' 'OB' # up
#bindkey -s '^l' '``[D'
bindkey -s '^l' '^uclear
'
bindkey '^a' _expand_alias

[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && source /opt/miniconda3/etc/profile.d/conda.sh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

