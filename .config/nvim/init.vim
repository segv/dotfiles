cnoremap W w
" set shortmess=a
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set exrc 
set clipboard+=unnamedplus
set guicursor=
set hidden
set noerrorbells
set nowrap
set termguicolors
set scrolloff=8
set colorcolumn=80
set updatetime=5
set lazyredraw
set pumheight=10
set number relativenumber
set nohlsearch
set smartcase
set ignorecase
set incsearch
set noswapfile
set nobackup
set undofile
set signcolumn=yes
" configure expanding of tabs for various file types
au BufRead,BufNewFile *.py set expandtab
au BufRead,BufNewFile *.c set expandtab
au BufRead,BufNewFile *.h set expandtab
au BufRead,BufNewFile Makefile* set noexpandtab

" --------------------------------------------------------------------------------
" configure editor with tabs and nice stuff...
" --------------------------------------------------------------------------------
set expandtab           " enter spaces when tab is pressed
" set textwidth=120       " break lines when line length increases
set tabstop=4           " use 4 spaces to represent tab
set softtabstop=4
set shiftwidth=4        " number of spaces to use for auto indent
set autoindent          " copy indent from current line when starting a new line

" make backspaces more powerfull
set backspace=indent,eol,start

call plug#begin()
Plug 'joshdick/onedark.vim'
Plug 'itchyny/lightline.vim'
Plug 'ervandew/supertab'
Plug 'jiangmiao/auto-pairs'
Plug 'wellle/targets.vim'
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'https://codeberg.org/segv/cmap.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'vim-python/python-syntax', {'for': 'python'}

Plug 'udalov/kotlin-vim'

Plug 'mattn/emmet-vim', {'for': 'html'}
Plug 'tpope/vim-surround'

" may or may not use these
Plug 'easymotion/vim-easymotion'
call plug#end()

colorscheme onedark
hi Normal guibg=NONE ctermbg=NONE
let g:lightline = {'colorscheme': 'one'}
let g:python_highlight_all = 1
let g:SuperTabDefaultCompletionType = "<c-n>"
let mapleader=' '
nnoremap <c-w> :w<cr>
nnoremap zz ZZ
nnoremap zq :q<cr>

nnoremap L $
nnoremap H 0

nnoremap zj ddpk
nnoremap zk ddkPj

vnoremap < <gv
vnoremap > >gv

map  <leader>f <plug>(easymotion-bd-f)
nmap <leader>f <plug>(easymotion-overwin-f)
nmap s <plug>(easymotion-overwin-f2)
map <leader>l <plug>(easymotion-bd-jk)
nmap <leader>l <plug>(easymotion-overwin-line)
map  <leader>w <plug>(easymotion-bd-w)
nmap <leader>w <plug>(easymotion-overwin-w)

nnoremap <c-b> :Buffers<cr>
nnoremap <c-e> :Files<cr>

autocmd FileType python setlocal completeopt-=preview
autocmd FileType python nnoremap <c-y> :0,$w! /tmp/codeforce.tmp <bar> !cfcp<cr><cr>
autocmd FileType python nnoremap <tab> :w<cr>:!tmux splitw -h sh -c "python %;echo press any key;read"<cr><cr>
autocmd FileType c,cpp nnoremap <c-y> <esc>$o
autocmd FileType cpp nnoremap <tab> :w<cr>:!tmux splitw -h sh -c "clang++ -O2 %;./a.out;rm -rf a.out;echo press any key;read"<cr><cr>
autocmd FileType c nnoremap <tab> :w<cr>:!tmux splitw -h sh -c "tcc -run %;echo press any key;read"<cr><cr>

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

autocmd FileType cpp inoremap ;CP <esc>ggVG:r /src/codeforces/start.cpp<cr><cr>gg/START<cr>cw
