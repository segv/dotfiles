#!/bin/make -f -
all: install

LOCAL_SHARE := unicode.txt applications chtshlist
CONFIG := htop keynav mimeapps.list neofetch nvim shell sxhkd wallpaper.png zsh nnn
CONFIG_IGNORE := .zcompdump packer_compiled.lua .netrwhist QtProject.conf urls mimeinfo.cache autoconfig.yaml plugged

.PHONY: install mkhomedir mkcurdir sync larbs
install: mkhomedir
	cp -rf .config/*	~/.config
	cp -rf .local/bin/*	~/.local/bin
	cp -rf .local/share/*	~/.local/share
	cp -f .tmux.conf	~/
	cp -f .xinitrc		~/
	ln -sf "$$HOME/.config/zsh/.zshrc" ~/.zprofile


sync: mkcurdir
	rm -rf .config/*
	rm -rf .local/bin/*
	rm -rf .local/share/*
	rm -f .tmux.conf .zprofile .xinitrc
	cp -rf ~/.local/bin/* .local/bin
	for c in $(CONFIG); do				\
		cp -rf ~/.config/$$c .config/;		\
	done
	for c in $(LOCAL_SHARE); do			\
		cp -rf ~/.local/share/$$c .local/share/;\
	done
	cd .config && for c in $(CONFIG_IGNORE); do	\
		find -type f -iname "$$c" -delete;	\
	done
	cp ~/.tmux.conf ~/.xinitrc .
	rm -rf .config/nvim/plugged

larbs:
	curl -LO larbs.xyz/larbs.sh
	chmod 755 larbs.sh
	t="$$(mktemp -d /tmp/nulgit.XXXXXXXXX)" &&	\
	cd "/tmp/$$t"				&&	\
	git init >/dev/null 2>&1		&&	\
	cd -					&&	\
	./larbs -r "/tmp/$$t" -p progs.csv -a yay &&	\
	rm -f progs.csv larbs.sh 		&& 	\
	rm -rf "$$t"



mkhomedir:
	mkdir -p ~/.config
	mkdir -p ~/.local
	mkdir -p ~/.local/share
	mkdir -p ~/.local/bin


mkcurdir:
	mkdir -p .config
	mkdir -p .local
	mkdir -p .local/share
	mkdir -p .local/bin
