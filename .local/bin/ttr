#!/bin/sh

set -e

quoteurl()
{
	curl -Gso /dev/null -w "%{url_effective}" --data-urlencode @- "" | cut -c 3-
}

usage()
{
cat << USAGE
Usage: ttr [OPTION]...
Translate text read from stdin.

OPTIONS:
	-T PORT		Use tor with PORT port
	-s LANG		Use LANG as the source language (default: auto)
	-t LANG		Use LANG as the target language (default: en)
	-c OPTS		Options to give to curl
	-m		Operate in multiline mode, end input with an empty line
USAGE
}

haslangerror()
{
	grep -q "Server got itself in trouble"
}

die()
{
	printf "error: %s\n" "$1" >&2
	exit 1
}

instance="https:/translate.metalune.xyz"
engine="google"
sourcelang="auto"
curlopts="--"
multiline=""

while getopts "mh::T:c:s:t:m:" o; do case "${o}" in
	T) curlopts="--proxy socks://localhost:$OPTARG $curlopts" ;;
	c) curlopts="$OPTARG $curlopts" ;;
	s) sourcelang="$OPTARG" ;;
	t) targetlang="$OPTARG" ;;
	m) multiline="yes" ;;
	h) usage && exit 0 ;;
	*) die "invalid option: -$OPTARG" ;;
esac done

intputtext=""
if [ -n "$multiline" ]; then
	while read -r input; do
		[ -z "$input" ] && break
		inputtext="$inputtext\n$input";
	done
else
	read -r inputtext
fi

response="$(curl "$instance/?engine=$engine&text=$(echo "$inputtext" | quoteurl)&sl=$sourcelang&tl=$targetlang" $(echo "$curlopts") 2>/dev/null)"
[ -z "$response" ] && die "empty response from the server (check your curl options and internet connection)"
echo "$response" | haslangerror && die "server got itself in trouble (check your language)"
echo "$response" | grep -zo "readonly>[^<]*</textarea>" | sed -z 's/readonly>//;s|</textarea>||'
